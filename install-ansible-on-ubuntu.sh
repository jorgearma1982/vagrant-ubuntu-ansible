#!/bin/bash

#
# script: install-ansible-on-ubuntu.sh
# Description: Setup ansible from PPA on Ubuntu Machine
# Author: Armando Medina
# Email: armando.medina@enova.mx
# Changelog: Initial version
#

#set -x

# vars

# main

echo
echo "Updating apt lists."
sudo apt-get -qq update
echo "Installing software-properties-common."
sudo apt-get -qq install software-properties-common

echo "Installing Ansible dependencies."
sudo apt-get install -qq -y python-yaml python-jinja2 python-paramiko python-crypto git vim

echo "Installing ansible from ansible ppa repository."
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get -qq update
sudo apt-get -qq install ansible
