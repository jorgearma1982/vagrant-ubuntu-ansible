# Automatically create local environments for development and testing using Vagrant on Ubuntu and Ansible

## Introduction

This document describes how to use Vagrant to create local environments ready
for development and testing activities.

Vagrant is a tool for building complete development environments. With an easy-to-use
workflow and focus on automation, Vagrant lowers development environment setup time,
increases development/production parity, and makes the "works on my machine" excuse
a relic of the past.

That is, you will have a local develpment environment with the same operating system,
libraries and configurations as in production. 

Using a local environment running on your machine is useful not only for developers
and testers who want to setup a development environment easily and start integrating
or testing changes continually, it is also useful for system administrators who
want to start working with new IT Automation practices like infrastructure as code,
so they as developers, can integrate infraestructure changes continually.

In this example we will create a local environment that is used for developing
Ansible playbooks that are part of the infrastructure's code, we will invoke ansible
as a local provisioner in vagrant.

This way, when we run a local environment, vagrant will create the local virtual
machines with virtualbox, and then provision the machines using ansible with a
playbook.

### Objetives

This is a proof of concept targeted to developers, QA analysys and infrastructure
sysadmins, the main objetives are:

- Define the baseline for servers based on the inmutable pattern.
- Allow developers to create repetible and reproducible development/testing
  environments on his machine.
- Automatically provision configurations on the virtual machines using Ansible.

Integrating this tools in the workflow we:

- Allow developers/QA/sysadmins to clone the proyect repository and start testing
 deployments on isolated environment.

## Setting up the local environment

On your local machine, Linux Machine for the moment, based on Debian, like Ubuntu,
Kubuntu or LinuxMint do the following:

- Install VirtualBox 4.x.
- Install Vagrant 1.7.x.

Once the vagrant setup is ready you need to clone the repository:

```
$ mkdir -p ~/data/vcs/enova
$ cd ~/data/vcs/enova
$ git clone http://git.enova.mx/infra/vagrant-ubuntu-ansible.git
```

Now continue setting up the virtual machines with vagrant.

## Vagrant Usage

### Initializing the local environment

Before you run your local environment, please import the image:

```
$ vagrant init ubuntu/trusty64
```

*NOTE:* The first time this can take a few minutes depending on your Internet connection.

### Running up the environment

After importing the image in the init phase, you can set up the local environment
with this command:

```
$ vagrant up
...
...
...
```

### Getting status information abaout the local environment

After running up the vagrant environment, you can see de virtual machines
running with:

```
$ vagrant status
Current machine states:

ansiblenode1              running (virtualbox)
managednode1              running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

### Connecting the local environment

You can connect by ssh to the nodes by using:

```
$ vagrant ssh ansiblenode1
```

or

```
$ vagrant ssh managednode1
```

You can logout from the machine by using the exit command.

And start commiting changes continually...

## Halting the local environment

You can halt the environment with the following command:

```
$ vagrant halt
==> managednode1: Attempting graceful shutdown of VM...
==> ansiblenode1: Attempting graceful shutdown of VM...
```

You can always shutdown individual machines, for example:

```
$ vagrant halt managednode1
==> managednode1: Attempting graceful shutdown of VM...
```

### Destroing the local environment

If you need to start over with a new environment, you can destroy the current
one with this command:

```
$ vagrant destroy
    managednode1: Are you sure you want to destroy the 'managednode1' VM? [y/N] y 
==> managednode1: Destroying VM and associated drives...
==> managednode1: Running cleanup tasks for 'ansible' provisioner...
    ansiblenode1: Are you sure you want to destroy the 'ansiblenode1' VM? [y/N] y
==> ansiblenode1: Destroying VM and associated drives...
==> ansiblenode1: Running cleanup tasks for 'shell' provisioner...
```

*NOTE:* Be careful when destroying your local environment, remember to commit and push your changes or backing up your settings.

### Start over the local environment

After halting or destroy the local environment, you can run the local environment
again with this command:

```
$ vagrant up
Bringing machine 'ansiblenode1' up with 'virtualbox' provider...
Bringing machine 'managednode1' up with 'virtualbox' provider...
...
...
...
```

### Provisioning the environment

If you want re run the provisioning module again, you can do it with this:

```
$ vagrant provision managednode1
...
...
...
```

## Running Ansible

If you want to run Ansible inside the ansiblenode1 machine, you do it like this:

```
$ vagrant ssh ansiblenode1
```

And then:

```
vagrant@ansiblenode1:~$ ansible-playbook --inventory="localhost," -c local /vagrant/playbook.yml
[DEPRECATION WARNING]: Instead of sudo/sudo_user, use become/become_user and 
make sure become_method is 'sudo' (default).
This feature will be removed in a 
future release. Deprecation warnings can be disabled by setting 
deprecation_warnings=False in ansible.cfg.

PLAY [all] *********************************************************************

TASK [setup] *******************************************************************
ok: [localhost]

TASK [update apt cache] ********************************************************
ok: [localhost]

TASK [install apache] **********************************************************
changed: [localhost]

TASK [install php] *************************************************************
changed: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=4    changed=2    unreachable=0    failed=0 
```

If you want to run ansible from you local machine, not inside the ansiblenode1 node,
you must have ansible installed on your local machine, you can use the script
install-ansible-on-ubuntu.sh.

```
[armando.medina@jmdev-at002corpo1][~/data/vcs/enova/vagrant-ubuntu-ansible]
$ PYTHONUNBUFFERED=1 ANSIBLE_FORCE_COLOR=true ANSIBLE_HOST_KEY_CHECKING=false ANSIBLE_SSH_ARGS='-o UserKnownHostsFile=/dev/null -o ControlMaster=auto -o ControlPersist=60s' ansible --private-key=/home/armando.medina/.vagrant.d/insecure_private_key --user=vagrant --connection=ssh --inventory-file=/home/armando.medina/data/vcs/enova/vagrant-ubuntu-ansible/.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory all -m ping
ansiblenode1 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
managednode1 | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}
```

'NOTE:' Remember to change the paths if your work directory is different.

## Refererences

Dejo aquí una lista de referencias web donde podrán conocer más de estas facinantes herramientas de virtualización, aprovisionamiento y administración de configuraciones.

Vagrant - Development environments made easy:
https://www.vagrantup.com/

VirtualBox - An x86 virtualization software package developed by Oracle:
https://www.virtualbox.org/

Ansible - Is simple IT Automation:
https://www.ansible.com/

Using Vagrant and Ansible:
http://docs.ansible.com/ansible/guide_vagrant.html

Vagrant & Ansible Quickstart Tutorial:
https://adamcod.es/2014/09/23/vagrant-ansible-quickstart-tutorial.html

Episode #45 - Learning Ansible with Vagrant (Part 2/4):
https://sysadmincasts.com/episodes/45-learning-ansible-with-vagrant-part-2-4

Ansible tutorial:
https://github.com/leucos/ansible-tuto

